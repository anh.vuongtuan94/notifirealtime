var crypto = require('crypto');
var Base64 = require('js-base64').Base64;
var serialize = require('php-serialize');


function decrypt(app, data) {
    if (data !== "") {
		try {
			var b64 = Base64.decode(data);
			var json = JSON.parse(b64);
			var iv = Buffer.from(json.iv, "base64");
			var value = Buffer.from(json.value, "base64");

			var keyword = Buffer.from(app, "utf8");
			// console.log(keyword)
			var decipher = crypto.createDecipheriv("aes-256-cbc", keyword, iv);

			// Decrypt
			var decrypted = decipher.update(value, 'binary', 'utf8');
			decrypted += decipher.final('utf8');

			// Unserialize
			unserialized = serialize.unserialize(decrypted);

			// Store in cache
			//console.log(unserialized);

			return unserialized;
		}
		catch(e) {
			console.log(e);
			return "";
		}
    }
    else {
        return "";
    }
}


module.exports = { decrypt }